//Thiha Min Thein
package backend;

import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random random;

    public RpsGame(){
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
        this.random = new Random();
    }

    public int getWins() {
        return wins;
    }

    public int getTies() {
        return ties;
    }

    public int getLosses() {
        return losses;
    }

    public String playRound(String choice){
        int cpuChoice = this.random.nextInt(3); //0 for rock, 1 for scissors, 2 for paper.
        String builder = "The computer plays";
        if(cpuChoice == 0){
            builder += " rock";
            switch (choice) {
                case "rock":
                    builder += " so it's a draw!";
                    this.ties++;
                break;
                case "scissors":
                    builder += " so the computer wins!";
                    this.losses++;
                break;
                case "paper":
                    builder += " so the player wins!";
                    this.wins++;
                break;
            }
        } else if (cpuChoice == 1){
            builder += " scissors";
            switch (choice) {
                case "rock":
                    builder += " so the player wins!";
                    this.wins++;
                break;
                case "scissors":
                    builder += " so it's a draw!";
                    this.ties++;
                break;
                case "paper":
                    builder += " so the computer wins!";
                    this.losses++;
                break;
            }
        } else {
            builder += " paper";
            switch (choice) {
                case "rock":
                    builder += " so the computer wins!";
                    this.losses++;
                break;
                case "scissors":
                    builder += " so the player wins!";
                    this.wins++;
                break;
                case "paper":
                    builder += " so it's a draw!";
                    this.ties++;
                break;
            }
        }
        return builder;
    }
}
