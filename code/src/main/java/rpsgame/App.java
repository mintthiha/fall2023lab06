//Thiha Min Thein
package rpsgame;

import java.util.Scanner;

import backend.RpsGame;

/**
 * Console application for rock paper scissors game
 *
 */
public class App 
{
    public static void main( String[] args )
    {   
        Scanner scan = new Scanner(System.in);
        String option = "";
        String choice = "";
        String message = "";
        RpsGame game = new RpsGame();
        while(!option.equals("no")){
            System.out.println("What action would you like to do? : ");
            choice = scan.nextLine();
            message = game.playRound(choice);
            System.out.println(message);
            System.out.println("");
            System.out.println("Would you like to play again?");
            option = scan.nextLine();
            if(option.equals("yes")){
                System.out.println("Onto the next round!");
                System.out.println();
            } else if (option.equals("no")){
                System.out.println("Alright, have a good day!");
            }
        }
        scan.close();
    }
}
